from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
    def test_solve_1(self):
        r = StringIO("A SanAntonio Hold\nB McAllen Move SanAntonio\nC Houston Move SanAntonio")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]')
    
    def test_solve_2(self):
        r = StringIO("B Dallas Move Austin\nF Austin Support B\nT Houston Support F")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'B [dead]\nF Austin\nT Houston')
        
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Barcelona Move Madrid\nD Austin Support C\nE SanAntonio Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB London\nC [dead]\nD Austin\nE SanAntonio')


if __name__ == "__main__": #pragma: no cover
    main()